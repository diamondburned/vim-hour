package main

import (
	"testing"
	"time"
)

// metrics integration test

func TestMetrics(t *testing.T) {
	// Set the time frame to a reasonable one for testing. Pretend 100ms is 100
	// seconds.
	TimeFrame = 100 * time.Millisecond // 100ms

	repo := &GitRepository{
		OriginURL: "git@gitlab.com:diamondburned/vim-hour.git",
	}

	// Run the single-frame test, which adds 100 seconds into the TotalTime
	// after typing for 10 seconds.
	t.Run("Single frame", testMetricsSingleFrame(repo))

	// Test that guarantees the results are fair even if
	t.Run("Old frames", testMetricsOldFrames(repo))
}

func testMetricsSingleFrame(r *GitRepository) func(t *testing.T) {
	return func(t *testing.T) {
		// Emulate typing for 10 seconds of 100 seconds.
		a := time.After(TimeFrame / 10)
	Typing:
		for {
			select {
			case <-a:
				break Typing
			default:
				// Type the shit out of the CPU cycles lmao
				r.Mark()
			}
		}

		r.bump()

		// At this point, the test should think that he's active for the first
		// frame, so he should now have 100 seconds added.
		if r.TotalTime != TimeFrame {
			t.Fatal("Unexpected result: " + r.TotalTime.String())
		}
	}
}

func testMetricsOldFrames(r *GitRepository) func(t *testing.T) {
	return func(t *testing.T) {
		// We wait 500 seconds, or for 5 frames.
		time.Sleep(TimeFrame * 5)

		// Type some more, pretend we're active after reading the docs.
		a := time.After(TimeFrame / 10)
	Typing:
		for {
			select {
			case <-a:
				break Typing
			default:
				r.Mark()
			}
		}

		r.bump()

		// At this point, we've been active for 2 frames in total, thus the total
		// time should be 2 frames, or 200 seconds.
		if r.TotalTime != 2*TimeFrame {
			t.Fatal("Unexpected result: " + r.TotalTime.String())
		}
	}
}
