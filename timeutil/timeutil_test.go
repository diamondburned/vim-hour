package timeutil

import (
	"testing"
	"time"
)

var (
	// 2019-10-10 01:49:01 +0000 UTC
	less = time.Unix(1570672141, 0)
	more = less.Add(time.Minute)
)

func TestLessThan(t *testing.T) {
	if !LessThan(less, more) {
		t.Fatal("Opposite result received")
	}
}

func TestMoreThan(t *testing.T) {
	if !MoreThan(more, less) {
		t.Fatal("Opposite result received")
	}
}

func TestLessThanOrEqual(t *testing.T) {
	if !LessThanOrEqual(less, more) {
		t.Fatal("Opposite result received")
	}

	if !LessThanOrEqual(more, more) {
		t.Fatal("Opposite result received")
	}
}

func TestMoreThanOrEqual(t *testing.T) {
	if !MoreThanOrEqual(more, less) {
		t.Fatal("Opposite result received")
	}

	if !MoreThanOrEqual(more, more) {
		t.Fatal("Opposite result received")
	}
}

func TestFloor(t *testing.T) {
	floor := Floor(less, time.Hour).UTC()

	if floor.Minute() != 0 {
		t.Fatal("Minute isn't 0")
	}

	if floor.Hour() != 1 {
		t.Fatal("Hour isn't 1")
	}
}

func TestCeil(t *testing.T) {
	ceil := Ceil(less, time.Hour).UTC()

	if ceil.Minute() != 0 {
		t.Fatal("Minute isn't 0")
	}

	if ceil.Hour() != 2 {
		t.Fatal("Hour isn't 2")
	}
}
