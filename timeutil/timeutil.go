package timeutil

import "time"

// return t1 <= t2
func LessThanOrEqual(t1, t2 time.Time) bool {
	return t1.Sub(t2) <= 0
}

// return t1 >= t2
func MoreThanOrEqual(t1, t2 time.Time) bool {
	return t1.Sub(t2) >= 0
}

// return t1 < t2
func LessThan(t1, t2 time.Time) bool {
	return t1.Sub(t2) < 0
}

// return t1 > t2
func MoreThan(t1, t2 time.Time) bool {
	return t1.Sub(t2) > 0
}

func Floor(t time.Time, prec time.Duration) time.Time {
	return t.Truncate(prec)
}

func Ceil(t time.Time, prec time.Duration) time.Time {
	return t.Truncate(prec).Add(prec)
}
