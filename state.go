package main

import (
	"bytes"
	"encoding/gob"
	"log"
	"sync"

	"github.com/neovim/go-client/nvim"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/clocker"
	"gitlab.com/diamondburned/vim-hour/bboltlock"
	"go.etcd.io/bbolt"
)

type State struct {
	// Repositories []*GitRepository
	// LastSaved    time.Time

	// writer *fl.File

	Debug bool

	db *bboltlock.DB
	mu sync.Mutex

	ticker *clocker.Ticker
	stopT  chan struct{}

	bumpMap map[nvim.Buffer]*GitRepository // map to repo
}

func LoadState(path string) (*State, error) {
	db, err := bboltlock.New(path, 0755, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to open the state database")
	}

	return &State{
		db:      db,
		stopT:   make(chan struct{}),
		bumpMap: map[nvim.Buffer]*GitRepository{},
	}, nil
}

// Start blocks.
func (s *State) Start() {
	s.ticker = clocker.NewTicker(TimeFrame)
	defer s.ticker.Stop()

	log.Println("Starting vim-hour...")

	// TODO: change this to a buffer change autocmd
	for {
		select {
		case <-s.stopT:
			return
		case <-s.ticker.C:
		}

		if err := s.db.Acquire(true, s.bumpRepositories); err != nil {
			log.Println("Failed to commit:", err)
		}
	}
}

func (s *State) Stop() {
	s.stopT <- struct{}{}
}

func (s *State) GetRepositories() ([]GitRepository, error) {
	var repos []GitRepository
	return repos, s.db.Acquire(true, func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("repositories"))
		if b == nil {
			return errors.New("vim-hour database is not initialized")
		}

		return b.ForEach(func(k, v []byte) error {
			var r GitRepository
			if err := gob.NewDecoder(bytes.NewReader(v)).
				Decode(&r); err != nil {

				return errors.Wrap(err, "Failed to decode "+r.OriginURL)
			}

			repos = append(repos, r)
			return nil
		})
	})
}

func (s *State) bumpRepositories(tx *bbolt.Tx) error {
	s.debugln("Acquired bbolt database lock")

	b, err := tx.CreateBucketIfNotExists([]byte("repositories"))
	if err != nil {
		return errors.Wrap(err, "Failed to create bucket")
	}

	for _, r := range s.bumpMap {
		if !r.marked {
			continue
		}

		bts := b.Get([]byte(r.OriginURL))
		if len(bts) == 0 {
			// Decode can fail, Bump can handle zero-value structs
			if err := gob.NewDecoder(bytes.NewReader(bts)).
				Decode(r); err != nil {

				return errors.Wrap(err, "Failed to decode "+r.OriginURL)
			}
		}

		r.bump()
		r.marked = false

		var buf bytes.Buffer
		if err := gob.NewEncoder(&buf).Encode(&r); err != nil {
			return errors.Wrap(err, "Failed to encode "+r.OriginURL)
		}

		return b.Put([]byte(r.OriginURL), buf.Bytes())
	}

	return nil
}

func (s *State) debugln(v ...interface{}) {
	if s.Debug {
		log.Println(v...)
	}
}

/*
func (s *State) BumpRepository(origin string, now time.Time) error {
	_origin := []byte(origin)

	ceil := timeutil.Ceil(now, TimeFrame)
	if !s.needsBump(origin, ceil) {
		return nil
	}

	return s.db.Acquire(true, func(tx *bbolt.Tx) (err error) {
		b := tx.Bucket([]byte("repositories"))

		var repo *GitRepository

		bts := b.Get(_path)
		if len(bts) == 0 {
			repo, err = New(path)
			if err != nil {
				return err
			}
		}

		// Decode can fail, Bump can handle zero-value structs
		if err := gob.NewDecoder(bytes.NewReader(bts)).Decode(repo); err != nil {
			return errors.Wrap(err, "Failed to decode "+path)
		}

		repo.Bump()
		s.bumpMap[path] = repo.LastFrame

		var buf bytes.Buffer
		if err := gob.NewEncoder(&buf).Encode(&repo); err != nil {
			return errors.Wrap(err, "Failed to encode "+path)
		}

		return b.Put(_path, buf.Bytes())
	})
}

func (s *State) needsBump(origin string, ceil time.Time) bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	t, ok := s.bumpMap[origin]
	if ok {
		if ceil.Equal(t) {
			return false
		}
	}

	return true
}
*/
