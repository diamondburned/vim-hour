package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/tabwriter"

	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var statePath = "~/.vim-hour"
var state *State

func main() {
	app := cli.NewApp()
	app.Name = "vim-hour"
	app.Usage = "offline vim time tracker in Go"
	app.Before = Init
	app.Action = Main

	app.Commands = []cli.Command{
		cli.Command{
			Name:   "top10",
			Action: Top10,
		},
		cli.Command{
			Name:   "manifest",
			Action: ManifestPlugin,
			Hidden: true,
		},
	}

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "state",
			Value: statePath,
		},
		cli.BoolFlag{
			Name: "debug",
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatalln(err)
	}
}

func Init(c *cli.Context) error {
	h, err := os.UserHomeDir()
	if err != nil {
		return errors.Wrap(err, "Failed to get user home directory")
	}

	statePath = c.String("state")
	statePath = strings.Replace(statePath, "~", h, 1)
	statePath = filepath.Clean(statePath)

	// fuck nvim/go-client
	flag.CommandLine = flag.NewFlagSet("", flag.ContinueOnError)
	flag.CommandLine.SetOutput(ioutil.Discard)

	s, err := LoadState(statePath)
	if err != nil {
		return errors.Wrap(err, "Failed to load state")
	}

	s.Debug = c.Bool("debug")

	state = s
	return nil
}

func Main(c *cli.Context) error {
	go state.Start()
	defer state.Stop()

	Initialize(state)

	return nil
}

func Top10(c *cli.Context) error {
	repos, err := state.GetRepositories()
	if err != nil {
		return errors.Wrap(err, "Failed to get repositories")
	}

	sort.Slice(repos, func(i, j int) bool {
		return repos[i].TotalTime < repos[j].TotalTime
	})

	if len(repos) > 10 {
		repos = repos[:10]
	}

	w := tabwriter.NewWriter(os.Stdout, 0, 4, 1, '\t', 0)

	for i, r := range repos {
		fmt.Fprintf(w, "%d\t%s\t%s", i+1, r.OriginURL, r.TotalTime.String())
	}

	return w.Flush()
}
