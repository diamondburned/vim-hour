module gitlab.com/diamondburned/vim-hour

go 1.13

require (
	github.com/blang/vfs v1.0.0
	github.com/davecgh/go-spew v1.1.1
	github.com/gofrs/flock v0.7.1
	github.com/neovim/go-client v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/urfave/cli v1.22.1
	gitlab.com/diamondburned/clocker v0.0.0-20190409150028-8b1f16fd9670
	go.etcd.io/bbolt v1.3.3
	gopkg.in/ini.v1 v1.48.0
)
