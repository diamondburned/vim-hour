package main

import (
	"fmt"

	"github.com/neovim/go-client/nvim"
	"github.com/neovim/go-client/nvim/plugin"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

func ManifestPlugin(c *cli.Context) error {
	p := plugin.New(nil)
	initializeFn(p)
	fmt.Println(string(p.Manifest("vim-hour")))
	return nil
}

var initializeFn = func(p *plugin.Plugin) error {
	p.HandleAutocmd(&plugin.AutocmdOptions{
		Event: "CursorMovedI", Pattern: "*", Group: "vim-hour",
	}, state.CursorMovedHandler)
	p.HandleAutocmd(&plugin.AutocmdOptions{
		Event: "BufFilePost", Pattern: "*", Group: "vim-hour",
	}, state.BufFileHandler)
	return nil
}

func Initialize(state *State) {
	plugin.Main(initializeFn)
}

func (s *State) BufFileHandler(v *nvim.Nvim, args []string) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	b, err := v.CurrentBuffer()
	if err != nil {
		return err
	}

	var path string

	// Does not exist, so we eval and get one
	if err := v.Eval(fmt.Sprintf(
		// Get the current working directory of the buffer.
		`echo expand("#%d:p")`, b), &path); err != nil {

		return err
	}

	r, ok := s.bumpMap[b]
	if ok && r.Path == path {
		return nil // unchanged git
	}

	// TODO: travel up for git
	r, err = New(path)
	s.bumpMap[b] = r

	return nil
}

func (s *State) CursorMovedHandler(v *nvim.Nvim, args []string) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	b, err := v.CurrentBuffer()
	if err != nil {
		return err
	}

	r, ok := s.bumpMap[b]
	if !ok {
		return errors.New("Buffer is not in cache")
	}

	if r == nil {
		return nil // not git repo, ignore
	}

	// Mark as busy
	r.Mark()

	return nil
}
