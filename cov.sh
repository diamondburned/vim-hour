#!/bin/sh

results=$(go test ./... --cover | tee /dev/stderr)

test_num=$(echo "$results" | wc -l)
percs=$(   echo "$results" | sed -E 's|.*coverage: (.*)% of statements|\1|')

printf "TEST_COVERAGE=%.2f%%" \
	"$(echo "( $(printf "%.1f + " $percs) 0.0 ) / $test_num" | bc -l)"

