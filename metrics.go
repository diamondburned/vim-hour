package main

import (
	"path/filepath"
	"time"

	"gitlab.com/diamondburned/vim-hour/timeutil"
)

type GitRepository struct {
	OriginURL string // unique identifier
	LastFrame time.Time
	TotalTime time.Duration

	// The old path
	Path string

	marked bool
}

// TimeFrame decides the global time frame to update the duration. The lower
// this number is, the more accurate it will be, trading off performance and
// resources.
var TimeFrame = 5 * time.Second

// New creates a new Git Repository tracker. dotGitPath points to the directory
// where '.git' exists.
func New(path string) (*GitRepository, error) {
	u, err := GetOrigin(filepath.Join(path, ".git", "config"))
	if err != nil {
		return nil, err
	}

	return &GitRepository{
		OriginURL: u,
		Path:      path,
	}, nil
}

// Mark is called to mark that the repository is being worked on.
func (g *GitRepository) Mark() {
	g.marked = true
}

func (g *GitRepository) bump() {
	// local check
	if !g.marked {
		return
	}

	now := time.Now()

	// round to timeframe
	c := timeutil.Ceil(now, TimeFrame)

	if !g.LastFrame.IsZero() && now.Before(g.LastFrame) {
		// Frame already marked, return
		return
	}

	// Add the estimated timeframe into the total time.
	g.TotalTime += TimeFrame

	// Set the LastFrame to the actual frame (t2). This will make the
	// background loop skip over this repository.
	g.LastFrame = c
}
