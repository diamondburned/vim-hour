// Package bboltlock provides an API that allows multiple processes to use the
// same database.
package bboltlock

import (
	"os"

	"go.etcd.io/bbolt"
)

type DB struct {
	path string
	mode os.FileMode
	opts *bbolt.Options
}

func New(path string, mode os.FileMode, options *bbolt.Options) (*DB, error) {
	// Try
	db, err := bbolt.Open(path, mode, options)
	if err != nil {
		return nil, err
	}

	if err := db.Close(); err != nil {
		return nil, err
	}

	return &DB{
		path: path,
		mode: mode,
		opts: options,
	}, nil
}

func (db *DB) Acquire(writable bool, f func(*bbolt.Tx) error) error {
	d, err := bbolt.Open(db.path, db.mode, db.opts)
	if err != nil {
		return err
	}

	defer d.Close()

	tx, err := d.Begin(writable)
	if err != nil {
		return err
	}

	defer tx.Rollback()

	if err := f(tx); err != nil {
		return err
	}

	return tx.Commit()
}
