if exists('g:loaded_vim_hour')
    finish
endif

let g:loaded_vim_hour = 1

" Helper functions from languageClient-neovim
function! s:err(message) abort
    echohl Error | echomsg "vim-hour: " . a:message | echohl None
endfunction

function! s:handler(job, lines, event) abort
	if a:event ==# 'stderr'
		call s:err(string(a:lines))
	elseif a:event ==# 'exit'
		call s:err('exited with: ' . string(a:lines))
	endif
endfunction

" Grab binary path
let s:bin_path = expand('<sfile>:p:h:h') . "/vim-hour"

function! s:Launch(host) abort
    return jobstart([ s:bin_path, '--debug' ], {
		\ 'rpc': v:true,
		\ 'on_stderr': function('s:handler'),
		\ 'on_exit':   function('s:handler')
		\ })
endfunction

call remote#host#Register('vim-hour', 'x', function('s:Launch'))
call remote#host#RegisterPlugin('vim-hour', '0', [
			\ {'type': 'autocmd', 'name': 'BufFilePost', 'sync': 1, 'opts': {'group': 'vim-hour', 'pattern': '*'}},
			\ {'type': 'autocmd', 'name': 'CursorMovedI', 'sync': 1, 'opts': {'group': 'vim-hour', 'pattern': '*'}},
			\ ])
