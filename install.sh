#!/usr/bin/env bash

# $1 URL
# $2 Destination
download() {
	command -v curl > /dev/null && curl \
		--fail --location "$1" --output "$2" && return

	command -v wget > /dev/null && wget \
		-O "$2" "$1" && return

	return 1
}

main() {
	# TODO: get CI up
	# test test, this is the vim-hour test.
	# the frame should be marked
	# insert stuff yadda yadda
	download "ayy lmao" "pogger.exe"
	return 0
}

main "$@"
